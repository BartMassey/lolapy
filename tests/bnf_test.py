import sys

show_table = False

sys.path = [".", "bootstrap"] + sys.path

import bnf_lexer
from bnf_lexer import Token
from lolapy import *
import llparse
import bnf

table = ParseTable(bnf.bnf)
if show_table:
    print(table)
llparse.parse(table.table, bnf_lexer.lexer(sys.argv[1]))
