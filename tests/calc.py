# Copyright © 2018 Bart Massey
# [This program is licensed under the GPL version 3 or later.]
# Please see the file LICENSE in the source
# distribution of this software for license terms.


# Example Dragon Book grammar pre-encoded for testing.

from lolapy import *

calc = Grammar([
    Prod(NT("expr"), [
        [NT("term"), NT("expr'")]
    ]),
    Prod(NT("expr'"), [
        [T("+"), NT("term"), NT("expr'")],
        []
    ]),
    Prod(NT("term"), [
        [NT("factor"), NT("term'")]
    ]),
    Prod(NT("term'"), [
        [T("*"), NT("factor"), NT("term'")],
        []
    ]),
    Prod(NT("factor"), [
        [T("("), NT("expr"), T(")")],
        [T("id")]
    ])
])
