import sys

show_table = False

sys.path = ["."] + sys.path

from lolapy import *
import llparse
import calc

class Token(object):
    def __init__(self, ttype, filename, value = None):
        self.ttype = ttype
        self.filename = filename
        self.line_number = 1
        self.value = value
        self.offset = None

    def __str__(self):
        result = "<" + self.ttype
        if self.value != None:
            result += " " + self.value
        result += ">"
        return result

def lexer(filename):
    def lex():
        with open(filename, "r") as f:
            line = next(f)
            words = line.split()
            for w in words:
                if w in {"(", ")", "*", "+"}:
                    yield Token(w, filename)
                    continue
                yield Token("id", filename, w)
    return lex

table = ParseTable(calc.calc)
if show_table:
    print(table)
llparse.parse(table.table, lexer(sys.argv[1]))
