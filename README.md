# lolapy: LL(1) predictive parsers
Copyright (c) 2018 Bart Massey

This code generates [will generate when complete] LL(1)
predictive parse tables for a given BNF grammar. It includes
[will include when complete] a sample table-driven parser in
Python.

There's a long history here, and a whole bunch of
explanation. I will write everything up more carefully when
I have a working program.

## License

This program is licensed under the GPL version 3 or later.
Please see the file `LICENSE` in the source distribution of
this software for license terms.
