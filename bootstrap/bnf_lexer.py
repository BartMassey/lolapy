# Copyright © 2018 Bart Massey
# [This program is licensed under the GPL version 3 or later.]
# Please see the file LICENSE in the source
# distribution of this software for license terms.


# Python BNF lexer. Reads a file containing a textual BNF
# grammar in BNF as input and generates tokens of the
# textual grammar.

import sys

sys.path = ["."] + sys.path

from llparse import LexException
import re

re_nt = re.compile(r"^[a-zA-Z_']+")
re_t = re.compile(r'^"([^" ]+)"')

# To work with the default parser, a token must have:
# * A "ttype" field indicating its token type.
# * A "value" field, which if not None indicates specifics
#   about the token.
# * "filename", "line_number", and "offset" fields
class Token(object):
    def __init__(self, ttype, value=None):
        self.line_number = None
        self.offset = None
        self.ttype = ttype
        self.value = value

    def __repr__(self):
        result = "<" + self.ttype
        if self.value != None:
            result += " " + repr(self.value)
        result += ">"
        return result

def enumerate_tokens(filename, line_number, line):
    posn = 0
    while posn < len(line):
        c = line[posn]
        if c == ' ':
            posn += 1
            continue
        if c == '\t':
            posn += 8 - posn % 8
            continue
        if c == '\n':
            return
        s = line[posn:].split(None, 1)[0]
        next_posn = posn + len(s)

        def parse_token(s):
            if s in {"::=", ";", "|"}:
                return Token(s)

            tm = re_t.match(s)
            if tm != None:
                return Token("t", tm[1])

            ntm = re_nt.match(s)
            if ntm != None:
                return Token("nt", ntm[0])

            raise LexException(
                filename,
                line_number + 1,
                posn,
                "{}: unrecognized token",
                s)

        t = parse_token(s)
        t.filename = filename
        t.line_number = line_number + 1
        t.offset = posn + 1
        yield t
        posn = next_posn
    
def lexer(filename):
    def lex():
        with open(filename, "r") as g:
            for line_number, line in enumerate(g):
                try:
                    c = line.index('#')
                    line = line[:c]
                except ValueError:
                    pass
                for t in enumerate_tokens(filename, line_number, line):
                    yield t
    return lex
