# Copyright © 2018 Bart Massey
# [This program is licensed under the GPL version 3 or later.]
# Please see the file LICENSE in the source
# distribution of this software for license terms.


# Left-factored non-left-recursive unambiguous bootstrap
# grammar for BNF. We will use this to parse the actual
# target grammar description, which during bootstrap will be
# a grammar for BNF in BNF.

from lolapy import *

bnf = Grammar([
    Prod(NT("grammar"), [
        [NT("prod"), NT("grammar")],
        []
    ]),
    Prod(NT("prod"), [
        [T("nt"), T("::="), NT("alts"), T(";")]
    ]),
    Prod(NT("alts"), [
        [NT("alt_ne"), NT("alts'")]
    ]),
    Prod(NT("alts'"), [
        [T("|"), NT("alts_ne")],
        []
    ]),
    Prod(NT("alts_ne"), [
        [NT("alts")],
        []
    ]),
    Prod(NT("alt"), [
        [NT("alt_ne")],
        []
    ]),
    Prod(NT("alt_ne"), [
        [T("t"), NT("alt")],
        [T("nt"), NT("alt")]
    ])
])
