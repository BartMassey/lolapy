# Copyright © 2018 Bart Massey
# [This program is licensed under the GPL version 3 or later.]
# Please see the file LICENSE in the source
# distribution of this software for license terms.


# Parse table generator for LL(1) predictive parser.

# A description of grammars.

# Grammar terminal symbol. We go to some trouble
# to make sure two different instantiations of
# a symbol with a particular name hash and compare
# equal, and that terminals and non-terminals do
# not.
class T(object):
    def __init__(self, name):
        self.name = name

    def is_terminal(self):
        return True

    def __eq__(self, other):
        return type(self) == type(other) and self.name == other.name

    def __hash__(self):
        return hash(type(self)) ^ hash(self.name)

    def __repr__(self):
        return "T(" + repr(self.name) + ")"

# Grammar nonterminal symbol. See note for terminal above.
class NT(object):
    def __init__(self, name):
        self.name = name

    def is_terminal(self):
        return False

    def __eq__(self, other):
        return type(self) == type(other) and self.name == other.name

    def __hash__(self):
        return hash(type(self)) ^ hash(self.name)

    def __repr__(self):
        return "NT(" + repr(self.name) + ")"

# Grammar production. LHS should be NT.  RHS should be a
# list of lists of T and NT representing alternatives.
class Prod(object):
    def __init__(self, lhs, rhss):
        self.lhs = lhs
        self.rhss = rhss

# Grammar, as a list of productions. The first production is
# presumed to be the start production: the grammar will be
# augmented with a new start symbol named "_start" producing
# that symbol and then a new terminal named "$" representing
# the end of the sentence.  The LHS of prods are presumed to
# be unique and to cover all NTs in the grammar. The grammar
# is presumed to be left-factored and left-recursion-free.
class Grammar(object):
    def __init__(self, prods):
        self.start = prods[0].lhs
        self.prods = {p.lhs : p.rhss for p in prods}

# An LL(1) parse table for a grammar. Built using Dragon
# Book algorithms.
class ParseTable():
    def __init__(self, grammar):
        prods = grammar.prods
        prods[NT("_start")] = [[grammar.start, T("$")]]
        nts = set()
        ts = set()
        for p, alts in prods.items():
            nts.add(p)
            for a in alts:
                for s in a:
                    if s.is_terminal():
                        ts.add(s)
                    else:
                        nts.add(s)

        # Initialize the FIRST set for nonterminals: will be
        # computed as we go.
        first = {p : set() for p in prods}

        # FIRST of a terminal is just itself.
        for t in ts:
            first[t] = {t}

        # FIRST of a non-terminal producing the empty
        # transition contains epsilon.  (We use None to
        # denote the empty (a.k.a. "epsilon") transition.)
        for p, alts in prods.items():
            for a in alts:
                if not a:
                    first[p].add(None)

        # Now do a transitive closure on the grammar.
        running = True

        # Iterate through the given alternative, adding
        # symbols until a symbol without an epsilon in FIRST
        # is encountered. Update running if new symbols were
        # added.
        def add_firsts(p, a):
            nonlocal running
            for s in a:
                eps = False
                for f in first[s]:
                    if not f:
                        eps = True
                        continue
                    if f not in first[p]:
                        first[p].add(f)
                        running = True
                if not eps:
                    break

        # The FIRST set for a production contains the FIRST
        # set of each alternative for that production.
        while running:
            running = False
            for p, alts in prods.items():
                for a in alts:
                    add_firsts(p, a)

        self.first = first

        # Initialize the FOLLOW set for nonterminals: will be
        # computed as we go.
        follow = {s: set() for s in nts}

        # Compute the FOLLOW set as the transitive closure
        # of the follow rules.
        running = True

        # Find the FIRST set for a sequence of symbols.
        def find_firsts(a):
            firsts = {None}
            for s in a:
                firsts |= first[s] - {None}
                if None not in first[s]:
                    firsts -= {None}
                    break
            return firsts

        while running:
            running = False
            for p, alts in prods.items():
                for a in alts:
                    for i, s in enumerate(a):
                        if s.is_terminal():
                            continue
                        firsts = find_firsts(a[i+1:])
                        follows = firsts - follow[s] - {None}
                        if follows:
                            follow[s] |= follows
                            running = True
                        if None in firsts and follow[p] - follow[s]:
                            follow[s] |= follow[p]
                            running = True

        self.follow = follow
  
        # Construct a predictive parse table. Fail if an
        # entry becomes ambiguous.
        
        table = dict()

        def add_entry(p, s, a):
            if a == []:
                a = None
            if (p, s) in table:
                if table[(p, s)] == a:
                    return
                raise Exception(
                    "ambiguous grammar: {} {} -> {} / {}".format(
                        p, s, table[(p, s)], a
                    )
                )
            table[(p, s)] = a

        for p, alts in prods.items():
            for a in alts:
                firsts = find_firsts(a)
                for s in firsts - {None}:
                    add_entry(p, s, a)
                if None in firsts:
                    for s in follow[p]:
                        add_entry(p, s, a)
        
        self.table = table

    def sets_str(self):
        result = ""

        def add_set(name, ss):
            nonlocal result
            result += name + "\n"
            ss = ["  " + str(s) + " : " + str(f)
                  for s, f in ss.items()]
            result += "\n".join(ss)

        add_set("first", {s: f for s, f in self.first.items()
                          if not s.is_terminal()})
        result += "\n\n"
        add_set("follow", self.follow)
        return result

    def __str__(self):
        ss = ["{} {}: {}".format(p, s, a)
              for (p, s), a in self.table.items()]
        return "\n".join(ss)
